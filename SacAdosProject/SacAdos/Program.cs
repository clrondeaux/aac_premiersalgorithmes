﻿using SacAdos.Models;
using SacAdos.Services;

class Program
{
    static void Main(string[] args)
    {
        List<Box> boxes = new List<Box>();
        boxes.Add(new Box { Weight = 95, Value = 55, WhatItIs = 'A' });
        boxes.Add(new Box { Weight = 4 , Value = 10, WhatItIs = 'B' });
        boxes.Add(new Box { Weight = 60, Value = 47, WhatItIs = 'C' });
        boxes.Add(new Box { Weight = 32, Value = 5,  WhatItIs = 'D' });
        boxes.Add(new Box { Weight = 23, Value = 4,  WhatItIs = 'E' });

        /*
        boxes.Add(new Box { Weight = 72, Value = 50, WhatItIs = 'F' });
        boxes.Add(new Box { Weight = 80, Value = 8,  WhatItIs = 'G' });
        boxes.Add(new Box { Weight = 62, Value = 61, WhatItIs = 'H' });
        boxes.Add(new Box { Weight = 65, Value = 85, WhatItIs = 'I' });
        boxes.Add(new Box { Weight = 46, Value = 87, WhatItIs = 'J' });
        */

        int weightMax = 100;

        SolutionEmpyrique.FindBest(boxes , weightMax);
    }
}