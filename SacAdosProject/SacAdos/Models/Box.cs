﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SacAdos.Models
{
    public class Box
    {
        /// <summary>
        /// The weight (in kg) this box/object have
        /// </summary>
        public int Weight { get; set; }

        /// <summary>
        /// The value (in euro) this box/object have
        /// </summary>
        public int Value { get; set; }    

        public char WhatItIs { get; set; }
    }
}
