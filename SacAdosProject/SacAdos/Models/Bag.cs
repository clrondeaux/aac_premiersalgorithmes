﻿namespace SacAdos.Models
{
    public class Bag
    {
        /// <summary>
        /// The maximum weight this bag can hold
        /// </summary>
        public int WeightMax { get; set; }

        /// <summary>
        /// The total number of boxes available
        /// Will define the lenght of the boxes list
        /// </summary>
        public int NbBoxes { get; set; }

        /// <summary>
        /// The final list of all boxes optimised by weight and price for the maximum weight this bag can hold
        /// </summary>
        public Box[] BoxList { get; private set; }

        /// <summary>
        /// Default constructor 
        /// </summary>
        /// <param name="nbBoxes"></param>
        public Bag(int nbBoxes , int weightMax) 
        {
            NbBoxes = nbBoxes ;
            BoxList = new Box[NbBoxes] ;
            WeightMax = weightMax;
        }

        public Bag(Bag b)
        {
            NbBoxes = b.NbBoxes ;
            BoxList = new Box[NbBoxes];
            for(int i = 0; i<NbBoxes; i++)
                BoxList[i]=b.BoxList[i];
            WeightMax = b.WeightMax ;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int ValueTotal()
        {
            int val = 0;
            foreach (Box box in BoxList)
                if (box != null)
                    val += box.Value;
            return val;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int WeightTotal()
        {
            int w = 0;
            foreach (Box box in BoxList)
                if (box != null)
                    w += box.Weight;
            return w;
        }

        public bool IsOverFilled()
        {
            if (WeightTotal() > WeightMax)
                return true;
            else
                return false;
        }

        public String WhatIsInside()
        {
            string s = "";
            foreach (Box b in BoxList)
                if (b != null)
                    s += b.WhatItIs + " ";
            return s;
        }
    }
}
