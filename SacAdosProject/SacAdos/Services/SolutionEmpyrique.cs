﻿using SacAdos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SacAdos.Services
{
    public class SolutionEmpyrique
    {
        public static Bag FindBest(List<Box> boxes , int weightMax)
        {
            Bag b = new Bag(boxes.Count , weightMax) ;
            Bag bestBag ;

            bestBag = Recursion(boxes , 0 , b);

            Console.WriteLine("----------------");
            DisplayRecursion(bestBag);
            Console.WriteLine("----------------");
            return bestBag;
        }


        private static Bag Recursion(List<Box> boxes , int iteration , Bag b)
        {
            Box box;
            Bag best = new Bag(b);

            for (int i = iteration; i < boxes.Count; i++)
            {
                box = boxes[i];
                if (!b.BoxList.Contains(box))
                { 
                    b.BoxList[iteration] = box;
                    DisplayRecursion(b);

                    if (!b.IsOverFilled() && b.ValueTotal() > best.ValueTotal())
                        best = new Bag(b) ;
                }
                Bag b2;
                if (!b.IsOverFilled() && iteration < boxes.Count)
                {
                    b2 = Recursion(boxes, iteration + 1, b);
                    if (!b2.IsOverFilled() && b2.ValueTotal() > best.ValueTotal() )
                        best = new Bag(b2) ;
                }

                b.BoxList[iteration] = null;
            }

            return best ;
        }

        private static void DisplayRecursion(Bag b)
        {
            if (b.IsOverFilled())
                Console.WriteLine(b.WhatIsInside() + "|" + b.WeightTotal() + "|");
            else
                Console.WriteLine(b.WhatIsInside() + "|" + b.WeightTotal() + "|-" + b.ValueTotal() + "- ");
        }
    }
}
