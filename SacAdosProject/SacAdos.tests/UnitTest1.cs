using Microsoft.VisualStudio.TestTools.UnitTesting;
using SacAdos.Models;
using SacAdos.Services;
using System.Collections.Generic;

namespace SacAdosTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            // Test 1 : Liste de bo�tes vide
            List<Box> emptyBoxes = new List<Box>();
            Bag best = new Bag(SolutionEmpyrique.FindBest(emptyBoxes, 10));
            Assert.AreEqual(0, best.ValueTotal());
            Assert.AreEqual(0, best.NbBoxes);
        }

        [TestMethod]
        public void TestMethod2()
        {
            // Test 2 : Une seule bo�te qui convient
            Box oneBox = new Box { Weight = 7, Value = 3, WhatItIs = 'A' };
            List<Box> oneBoxList = new List<Box> { oneBox };
            Bag best = SolutionEmpyrique.FindBest(oneBoxList, 10);
            Assert.AreEqual(3, best.ValueTotal());
            Assert.AreEqual(7, best.WeightTotal());
        }

        [TestMethod]
        public void TestMethod3()
        {
            // Test 3 : Une seule bo�te qui ne convient pas
            Box oneBox2 = new Box { Weight = 15, Value = 1, WhatItIs = 'A' };
            List<Box> oneBoxList2 = new List<Box> { oneBox2 };
            Bag best = SolutionEmpyrique.FindBest(oneBoxList2, 10);
            Assert.AreEqual(0, best.ValueTotal());
            Assert.AreEqual(0, best.WeightTotal());
        }

        [TestMethod]
        public void TestMethod4()
        {
            // Test 3 : Une seule bo�te �gale � la capacit� max
            Box oneBox2 = new Box { Weight = 10, Value = 1, WhatItIs = 'A' };
            List<Box> oneBoxList2 = new List<Box> { oneBox2 };
            Bag best = SolutionEmpyrique.FindBest(oneBoxList2, 10);
            Assert.AreEqual(1, best.ValueTotal());
            Assert.AreEqual(10, best.WeightTotal());
        }

        [TestMethod]
        public void TestMethod5()
        {
            // Test 4 : Plusieurs bo�tes qui conviennent, avec des valeurs diff�rentes, le meilleur r�sultat doit �tre les trois dans le sac
            Box box1 = new Box { Weight = 10, Value = 3, WhatItIs = 'A' };
            Box box2 = new Box { Weight = 20, Value = 8, WhatItIs = 'B' };
            Box box3 = new Box { Weight = 30, Value = 5, WhatItIs = 'C' };
            List<Box> boxesList = new List<Box> { box1, box2, box3 };
            Bag best = SolutionEmpyrique.FindBest(boxesList, 60);
            Assert.AreEqual(16, best.ValueTotal());
            Assert.AreEqual(60, best.WeightTotal());
        }

        [TestMethod]
        public void TestMethod6()
        {
            // Test 5 : Plusieurs bo�tes qui ne conviennent pas en raison de leur poids
            Box box4 = new Box { Weight = 12, Value = 3, WhatItIs = 'A' };
            Box box5 = new Box { Weight = 15, Value = 3, WhatItIs = 'B' };
            List<Box> boxesList2 = new List<Box> { box4, box5 };
            Bag best = SolutionEmpyrique.FindBest(boxesList2, 10);
            Assert.AreEqual(0, best.ValueTotal());
            Assert.AreEqual(0, best.WeightTotal());
        }

        [TestMethod]
        public void TestMethod7()
        {
            // Test 6 : Plusieurs bo�tes dont certaines conviennent et d'autres pas
            Box box6 = new Box { Weight = 4, Value = 5, WhatItIs = 'A' };
            Box box7 = new Box { Weight = 5, Value = 4, WhatItIs = 'B' };
            Box box8 = new Box { Weight = 3, Value = 3, WhatItIs = 'C' };
            Box box9 = new Box { Weight = 7, Value = 9, WhatItIs = 'D' };
            List<Box> boxesList3 = new List<Box> { box6, box7, box8, box9 };
            Bag best = SolutionEmpyrique.FindBest(boxesList3, 10);
            Assert.AreEqual(12, best.ValueTotal());
            Assert.AreEqual(10, best.WeightTotal());
        }

        [TestMethod]
        public void TestMethod8()
        {
            // Test 7 : Bo�tes avec poids et valeurs �gaux
            Box box10 = new Box { Weight = 5, Value = 5, WhatItIs = 'A' };
            Box box11 = new Box { Weight = 5, Value = 5, WhatItIs = 'B' };
            Box box12 = new Box { Weight = 5, Value = 5, WhatItIs = 'C' };
            List<Box> boxesList4 = new List<Box> { box10, box11, box12 };
            Bag best = SolutionEmpyrique.FindBest(boxesList4, 10);
            Assert.AreEqual(10, best.ValueTotal());
            Assert.AreEqual(10, best.WeightTotal());
        }
    }
}
