#include <anagram.hpp>


//cette fonction permet de passer sur une chaine de caractère et d'en supprimer les doublons
void remove(vector<string> &chaine)
{
    auto end = chaine.end();
    for (auto it = chaine.begin(); it != end; ++it)
        end = remove(it + 1, end, *it);
        
    chaine.erase(end, chaine.end());
}

//l'objectif est d'utiliser un dictionnaire qui associe à chaque signature une liste des mots qui ont cette signature
map<string,vector<string>> anagram_in_phrase(vector<string> &chaine)
{
    //on enlève les doublons de la chaîne
    remove(chaine);
    //le dictionnaire est une map qui fait correspondre la signature d'un mot 
    //et la liste des mots qui ont la même signature
    map <string,vector<string>> dico;
    
    for(auto const & it : chaine)
    {
        //on stocke le mot courant
        string s = it;
        //on trie par ordre alphabétique le mot courant = c'est la signature
        sort(s.begin(), s.end());
        //si le mot a la même signature qu'un autre, on le mettra dans la liste correspondante à la signature
        //sinon ça crée une nouvelle entrée de dictionnaire
        dico[s].push_back(it);
    }
    return dico;
}

// Cette fonction liste les anagrammes d'une phrase à partir des mots qu'on peut trouver dans elle même
vector<vector<string>> Anagram::AnagramDansPhrase(string phrase)
{
    //fonctionnement de l'algorithme sur une phrase avec plusieurs anagrammes
    vector<string> chaine;

    //préparation de la phrase pour être dans une liste, séparation selon les espaces
    istringstream iss(phrase);
    copy(istream_iterator<string>(iss),
    istream_iterator<string>(),
    back_inserter(chaine));

    //on genère le dictionnaire associé à la phrase
    map<string,vector<string>> dico_phrase = anagram_in_phrase(chaine);
    vector<vector<string>> result ;
    vector<string> temp ;

    //affichage des anagrammes
    cout<<"Pour la phrase : " << phrase << "\nLa liste des anagrammes est : "<<endl;
    map <string,vector<string>>::iterator it;
    for (it = dico_phrase.begin (); it != dico_phrase.end(); it++)
    {
        if ((it->second).size() > 1) //on veut ignorer les mots sans anagrammes
        {
            cout << "{";
            temp.clear() ;
            for (auto const & s : it->second)
            {
                cout<<s<<" - ";
                temp.push_back(s) ;
            }
            result.push_back(temp) ;
            cout << "}\n";
        }
    }

    return result ;
}

// Cette fonction liste les anagrammes d'un mot à partir d'un dictionnaire français de mots
vector<string> Anagram::AnagramDansDico(string mot)
{
    vector<string> liste_mots;

    string const nomFichier("../liste_francais.txt");
    ifstream monFlux(nomFichier.c_str());

    if(monFlux)
    {
        string ligne;
        while (getline(monFlux, ligne, ' '))
        {   
            liste_mots.push_back(ligne);
        }
    }
    else
    {
        cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
    }

    //on genère le dictionnaire associé à la liste des mots donnée
    map<string,vector<string>> dico = anagram_in_phrase(liste_mots);

    //saisie d'un mot dont on veut les anagrammes
    sort(mot.begin(), mot.end());

    //si on trouve la signature du mot dans le dictionnaire
    map<string,vector<string>>::iterator find;
    find = dico.find(mot);
    vector<string> result ;
    
    //on afficher le résultat
    if (find != dico.end()) 
    {
        cout << "Voici la liste des réponses : " << endl;
        for (auto const & it : find->second)
        {
            if (it != "")
                result.push_back(it) ;
        }
    }
    else
    {
        cout << "Le mot n'a pas été trouvé dans le dictionnaire";
    }
    
   return result ;
}
