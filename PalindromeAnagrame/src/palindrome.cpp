/* -------------------------------------------------------------------------- */
/**/
/* -------------------------------------------------------------------------- */

#include <palindrome.hpp>



// on prépare le mot en ajoutant les symboles pour le traitement par l'algorithme de manacher
// l'objectif est de pouvoir traiter de manière égale les palindromes de longueur paire et de longueur impaire
string prepareMot(const string &s)
{
    string new_s = "@";

    for (int i = 0; i < s.size(); i++)
        new_s += "#" + s.substr(i, 1);

    new_s += "#$";

    return new_s;
}


// application de l'algorithme de manacher sur le mot choisi, retourne le plus long palindrome trouvé dans le mot
string palindrome::manacher(const string &s)
{
    // vérifier si dans s il n'y a pas déjà les caractères suivants '$''^','#'
    std::size_t trouve1 = s.find("$");
    std::size_t trouve2 = s.find("@");
    std::size_t trouve3 = s.find("#");
    if (trouve1 != string::npos || trouve2 != string::npos || trouve3 != string::npos || s == "")
    {
        cout<< ("erreur, caractères $, @ ou # présents dans le mot donné ou mot vide\n");
        return "" ;
    }

    // c centre du palindrome
    int c = 0;
    // d distance c+r r rayon du palindrome, soit d indice de fin du palindrome
    int d = 0;
    // tableau p qui indique pour chaque position i le plus grand rayon r tel que i-r à i+r soit un palindrome
    int p[SIZE];
    int maxPalindrome = 0;
    int centrePalin = 0;

    string new_s = prepareMot(s);

    for (int i = 1; i < new_s.size() - 1; i++)
    {
        // reflet de l'indice de i par rapport au centre
        int miroir = 2 * c - i;
        p[i] = max(0, min(d - i, p[miroir]));
        // on agrandi le palindrome centré en i
        while (new_s[i + 1 + p[i]] == new_s[i - 1 - p[i]])
        {
            p[i] += 1;
        }
        if (i + p[i] > d)
        {
            c = i;
            d = i + p[i];
        }
    }

    for (int j = 1; j < new_s.size() - 1; j++)
    {
        if (p[j] > maxPalindrome)
        {
            maxPalindrome = p[j];
            centrePalin = j;
        }
    }

    // recuperation de la solution
    return s.substr((centrePalin - 1 - maxPalindrome) / 2, maxPalindrome);
}
