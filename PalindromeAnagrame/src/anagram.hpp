#ifndef Anagram__
#define Anagram__

#include <sstream>
#include <iterator>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <fstream>
using namespace std;

class Anagram
{
    public :
        vector<vector<string>> static AnagramDansPhrase(string phrase) ;
        vector<string> static AnagramDansDico(string mot) ;
} ;

#endif // Anagram__