// Entetes //---------------------------------------------------------------------------------------
#include "catch.hpp"
#include <cmath>
#include <anagram.hpp>
#include <palindrome.hpp>

// Tests //-----------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------ 1
template<typename T>
bool isEqual(std::vector<T> const &v1, std::vector<T> const &v2)
{
    return (v1.size() == v2.size() &&
            std::equal(v1.begin(), v1.end(), v2.begin()));
}

// -------------------------------------------------------------------------
//                          AnagramDansPhrase
// -------------------------------------------------------------------------
TEST_CASE ( "Phrase vide" ) 
{
    vector<vector<string>> result = Anagram::AnagramDansPhrase("") ;
    REQUIRE(result.empty()) ;   
}

TEST_CASE ( "Phrase non régression" ) 
{
    vector<vector<string>> result = Anagram::AnagramDansPhrase("le chien marche vers sa niche et trouve une limace de chine nue pleine de malice qui lui fait du charme") ;
    REQUIRE(!result.empty()) ;

    REQUIRE(isEqual(result[0] , {"marche" , "charme"}));
    REQUIRE(isEqual(result[1] , {"limace" , "malice"})) ;
    REQUIRE(isEqual(result[2] , {"chien" , "niche" , "chine"})) ;
    REQUIRE(isEqual(result[3] , {"une" , "nue"}));
}

TEST_CASE ( "Phrase existe pas" ) 
{
    vector<vector<string>> result = Anagram::AnagramDansPhrase("le efrhje zehfrbjheqkazd sqdqsd") ;
    REQUIRE(result.empty()) ;
}

// -------------------------------------------------------------------------
//                          AnagramDansDico
// -------------------------------------------------------------------------
TEST_CASE ( "Mot vide" ) 
{
    auto result = Anagram::AnagramDansDico("") ;
    REQUIRE(result.empty()) ;
}

TEST_CASE ( "Mot existe pas" ) 
{
    auto result = Anagram::AnagramDansDico("zdefdguhejzbdesiufhygvszqhdyfsvzqhdbgehbgv") ;
    REQUIRE(result.empty()) ;
}

TEST_CASE ( "Mot non régression" ) 
{
    auto result = Anagram::AnagramDansDico("chien") ;
    
    REQUIRE (result.size() == 2) ;
    REQUIRE( result[0] == "chien" ) ;
    REQUIRE( result[1] == "chine" ) ;
}


// -------------------------------------------------------------------------
//                              Palindrome
// -------------------------------------------------------------------------
TEST_CASE ( "mot vide" ) 
{
    auto result =  palindrome::manacher("") ;
    REQUIRE( result == "" ) ;
}

TEST_CASE ( "mot sans palindrome" ) 
{
    auto result =  palindrome::manacher("qfzdio") ;
    REQUIRE( result == "q" ) ;
}

TEST_CASE ( "trouver un palindrome" ) 
{
    auto result =  palindrome::manacher("qsddazzaqsedrtyu") ;
    REQUIRE( result == "azza" ) ;
}

TEST_CASE ( "trouver le meilleur palindrome" ) 
{
    auto result =  palindrome::manacher("qsddazzaqsedzzuuzzrtyu") ;
    REQUIRE( result == "zzuuzz" ) ;
}

TEST_CASE ( "gestion des majuscules" ) 
{
    auto result =  palindrome::manacher("Radar") ;
    REQUIRE( result == "ada" ) ;
}

TEST_CASE ( "gestion des caractères spéciaux" ) 
{
    auto result =  palindrome::manacher("sgsdf@qs#") ;
    REQUIRE( result == "" ) ;
}
